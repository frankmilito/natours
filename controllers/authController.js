const User = require('../models/userModel');
const AppError = require('../utils/appError');
const catchAsync = require('../utils/catchAsync');
const jwt = require('jsonwebtoken');
const { promisify } = require('util');
const Email = require('../utils/email');
const crypto = require('crypto');

const signToken = (id) => {
  const token = jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });
  return token;
};

const createSignToken = (statusCode, user, res) => {
  const token = signToken(user._id);

  const cookeOptions = {
    expires: new Date(
      Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000
    ),
    httpOnly: true,
  };

  //Remove password from the output
  user.password = undefined;
  if (process.env.NODE_ENV === 'production') cookeOptions.secure = true;
  res.cookie('jwt', token, cookeOptions);
  return res.status(statusCode).json({
    status: 'success',
    token,
    data: {
      user,
    },
  });
};
exports.signup = catchAsync(async (req, res) => {
  const newUser = await User.create({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
    passwordConfirm: req.body.passwordConfirm,
    role: req.body.role,
  });

  const url = `${req.protocol}://${req.get('host')}/me`;
  console.log(url);
  await new Email(newUser, url).sendWelcome();
  createSignToken(200, newUser, res);
});

exports.login = catchAsync(async (req, res, next) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return next(new AppError('Please provide email and password', 400));
  }

  const user = await User.findOne({ email }).select('+password');
  // const correct = await user.correctPassword(password, user.password);
  console.log(user);
  if (!user || !(await user.correctPassword(password, user.password))) {
    return next(new AppError('Email or password provided is invalid', 401));
  }
  createSignToken(200, user, res);
});

exports.protectedRoute = catchAsync(async (req, res, next) => {
  //get token and confirming
  let token;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer')
  ) {
    token = req.headers.authorization.split(' ')[1];
  } else if (req.cookes.jwt) {
    token = req.cookes.jwt;
  }
  if (!token) {
    return next(new AppError('Unauthorized Access', 401));
  }

  //verify the token
  const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);

  //check if the user still exists/ has not been deleted
  const existingUser = await User.findById(decoded.id);
  if (!existingUser) {
    return next(new AppError('This user does not exist', 401));
  }

  //Check if the user changed their password after token was issued
  if (existingUser.changedPasswordAfter(decoded.iat)) {
    return next(new AppError('User recently changed password', 401));
  }
  req.user = existingUser;
  next();
});

// Only for rendered pages, no errors!
exports.isLoggedIn = async (req, res, next) => {
  if (req.cookies.jwt) {
    try {
      // 1) verify token
      const decoded = await promisify(jwt.verify)(
        req.cookies.jwt,
        process.env.JWT_SECRET
      );

      // 2) Check if user still exists
      const currentUser = await User.findById(decoded.id);
      if (!currentUser) {
        return next();
      }

      // 3) Check if user changed password after the token was issued
      if (currentUser.changedPasswordAfter(decoded.iat)) {
        return next();
      }

      // THERE IS A LOGGED IN USER
      res.locals.user = currentUser;
      return next();
    } catch (err) {
      return next();
    }
  }
  next();
};
exports.restrictTo =
  (...roles) =>
  (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      return next(
        new AppError('You do not have permission to perform this action', 403)
      );
    }
    next();
  };

exports.forgotPassword = catchAsync(async (req, res, next) => {
  // 1) Get user based on email sent

  const user = await User.findOne({ email: req.body.email });
  if (!user) {
    return next(new AppError('There is no user with that email', 404));
  }
  // // 2) Generate random reset token
  const resetToken = user.createPasswordResetToken();
  await user.save({ validateBeforeSave: false });
  // next();

  // 3) Send it to users email

  try {
    const resetURL = `${req.protocol}://${req.get(
      'host'
    )}/api/v1/users/reset-password/${resetToken}`;
    await new Email(user, resetURL).sendPasswordReset();

    res.status(200).json({
      status: 'success',
      message: 'Token sent to email',
    });
  } catch (err) {
    user.passwordResetToken = undefined;
    user.passwordResetExpire = undefined;

    await user.save({
      validateBeforeSave: false,
    });
    console.log(err);
    return next(
      new AppError(
        'There was an error sending email. Please try again later',
        500
      )
    );
  }
});
exports.resetPassword = catchAsync(async (req, res, next) => {
  //!1) Get user based on token
  // console.log(req.params.token);
  const hashedToken = crypto
    .createHash('sha256')
    .update(req.params.token)
    .digest('hex');

  const user = await User.findOne({
    passwordResetToken: hashedToken,
    passwordResetExpire: { $gt: Date.now() },
  });

  //2) If token has not expired and there is a user, set new password
  if (!user) {
    return next(new AppError('Token is invalid or expired', 400));
  }

  //3) Update changedPasswordAt for the user
  user.password = req.body.password;
  user.passwordConfirm = req.body.passwordConfirm;
  user.passwordResetExpire = undefined;
  user.passwordResetToken = undefined;
  await user.save();
  //4) Log the user in send JWT
  createSignToken(200, user, res);
});

exports.updatePassword = catchAsync(async (req, res, next) => {
  //1) Get user from the collection
  const { oldPassword, newPassword, passwordConfirm } = req.body;
  if (!oldPassword || !newPassword || !passwordConfirm) {
    return next(new AppError('Please provide all details', 400));
  }

  const user = await User.findById(req.user._id).select('+password');
  //2) Check if the posted current user is correct
  // console.log(newUser);
  // if (!newUser) {
  //   return next(new AppError('You current password is incorrect', 400));
  // }

  if (!(await user.correctPassword(oldPassword, user.password))) {
    return next(new AppError('You current password is incorrect', 400));
  }

  //3) If so update the passwords
  user.password = newPassword;
  user.passwordConfirm = passwordConfirm;
  await user.save();
  // //4) Log user in, send JWT
  // const token = signToken(newUser._id);

  createSignToken(200, user, res);
});
